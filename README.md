## how to build
docker build --no-cache=true --build-arg BUILD_DATE=$(date -u +'%Y-%m-%dT%H:%M:%SZ') -t innovatetech/aws-cli-docker .

## how to push after loging in to docker hub
docker push innovatetech/aws-cli-docker